import Nav from './Nav';
import AttendeesList from './AttendeesList.js';
import LocationForm from './LocationForm.js';
import ConferenceForm from './ConferenceForm.js';
import AttendConferenceForm from './AttendConferenceForm.js';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';
import {
  Route,
  BrowserRouter,
  Routes
} from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
        <Nav />
          <div className="container">
            <Routes>
              <Route index element={<MainPage />} />
              <Route path="locations">
                <Route path="new" element={<LocationForm />} />
              </Route>
              <Route path="conferences">
                <Route path="new" element={<ConferenceForm />} />
              </Route>
              <Route path="attendees">
                <Route path="" element={<AttendeesList attendees={props.attendees}/>} />
                <Route path="new" element={<AttendConferenceForm />} />
              </Route>
              <Route path="presentations">
                <Route path="new" element={<PresentationForm />} />
              </Route>
            </Routes>
          </div>
    </BrowserRouter>
  );
}

export default App;
