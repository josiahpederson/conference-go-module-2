import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            description: "",
            maxAttendees: "",
            maxPresentations: "",
            starts: "",
            ends: "",
            locations: [],
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleStartTimeChange = this.handleStartTimeChange.bind(this);
        this.handleEndTimeChange = this.handleEndTimeChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees;
        delete data.maxAttendees;
        delete data.maxPresentations;
        delete data.locations;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

          const cleared = {
            name: "",
            description: "",
            maxPresentations: "",
            maxAttendees: "",
            starts: "",
            ends: "",
            location: "",
          };
          this.setState(cleared);
        }    
    }

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value})
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({maxPresentations: value})
    }

    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({maxAttendees: value})
    }

    handleStartTimeChange(event) {
        const value = event.target.value;
        this.setState({starts: value})
    }

    handleEndTimeChange(event) {
        const value = event.target.value;
        this.setState({ends: value})
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }

    async componentDidMount() {
        const url = "http://localhost:8000/api/locations";

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            this.setState({locations: data.locations})
        }
    }

    render () {
        return (
            <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a new conference</h1>
                <form onSubmit={this.handleSubmit} id="create-conference-form">
                  <div className="form-floating mb-3">
                    <input onChange={this.handleNameChange} placeholder="Name"
                        required type="text" id="name" className="form-control"
                        name="name" value={this.state.name}/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="mb-3">
                      <textarea onChange={this.handleDescriptionChange} placeholder="Description" 
                        required type="text" id="description" className="form-control" 
                        name="description" rows="3" value={this.state.description}></textarea>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleMaxPresentationsChange} placeholder="Max Presentations" 
                        required type="number" id="max_presentations" className="form-control" 
                        name="max_presentations" value={this.state.maxPresentations}/>
                    <label htmlFor="max_presentations">Max Presentations</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleMaxAttendeesChange} placeholder="Max Attendees"
                        required type="number" id="max_attendees" className="form-control" 
                        name="max_attendees" value={this.state.maxAttendees}/>
                    <label htmlFor="max_attendees">Max Attendees</label>
                  </div>
    
                  <div className="form-floating mb-3">
                    <input onChange={this.handleStartTimeChange} placeholder="Start Date"
                        type="date" id="starts" className="form-control" name="starts"  value={this.state.starts}/>
                    <label htmlFor="starts">Starts</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={this.handleEndTimeChange} placeholder="End Date" 
                        type="date" id="ends" className="form-control" name="ends"  value={this.state.ends}/>
                    <label htmlFor="ends">Ends</label>
                  </div>
    
                  <div className="mb-3">
                    <select onChange={this.handleLocationChange} required id="location"
                        className="form-select" name="location" value={this.state.location}>
                      <option value="">Choose a location</option>
                      {this.state.locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                                {location.name}
                            </option>
                        );
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        )
    }
}

export default ConferenceForm;